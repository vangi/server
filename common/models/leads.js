'use strict';
var request = require('request');
module.exports = function(Leads) {
    Leads.observe('after save', async (ctx) => {
        console.log('> after save triggered:', ctx.Model.modelName, ctx.instance);
        console.log('######### TEST #########');
        const API_URL = 'https://www.rdstation.com.br/api/1.3/conversions';
        const API_TOKEN = '62d9c778fa41b8ad8d694143731ceb6d';
        const data = {
            token_rdstation: API_TOKEN,
            identificador: "Test API",
            email: ctx.instance.email,
            telefone: ctx.instance.telefone,
        };
        console.log(data);
        request({
            'method': 'POST', 
            'url': API_URL,
            'headers': {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            'json': data
        }, function(error, response, body) {
            if(error) {
                console.log('Error => ', error);
            } else {
                console.log(response.statusCode, body);
            }
        })
        return;
    });
};
